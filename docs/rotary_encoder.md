
https://www.amazon.ca/gp/product/B08HVC4J5Y/

#### Description
Incremental Rotary Encoder, 600P/R Photoelectric Optical Rotation Encoder 5V-24V Wide Voltage Power Supply 6mm Shaft, Rotary Encoder Module 


##### [600P/R-600 pulses per Revolution]: 
This product has 600 p/r 
* Single‑phase 600 pulses /R, 2 phase 4 frequency doubling to 2400 pulses
* By rotating the grating disk and optocoupler direction, this incremental optical rotary encoder generate counting pulses

##### Power source is DC5‑24V. 

##### Output 
* AB 2-phase output rectangular orthogonal pulse circuit, the output for the NPN open collector output type

##### Main Function 
The incremental rotary encoder can be used to measure the rotational rate, Angle and acceleration of the object and the length measurement.

##### Application 

Suitable for intelligent control of various displacement measurement, automatic fixed‑length leather automatic guillotine machines, steel cut length control, height and weight scale, students racing robots.



#### Color code

* green = A phase 
* white = B phase
* red = Vcc power +
* black = V0
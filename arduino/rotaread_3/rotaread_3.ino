// -----
//
// a partir d'ici
// connecter
// v2 ethernet
// v3 ethernet 3
///
// AcceleratedRotator.ino - Example for the RotaryEncoder library.
// This class is implemented for use with the Arduino environment.
// Copyright (c) by Matthias Hertel, http://www.mathertel.de
// This work is licensed under a BSD style license. See http://www.mathertel.de/License.aspx
// More information on: http://www.mathertel.de/Arduino
// -----
// 18.01.2014 created by Matthias Hertel
// 13.11.2019 converted to AcceleratedRotator by Damian Philipp
// 06.02.2021 conditions and settings added for ESP8266
// -----

// This example checks the state of the rotary encoder in the loop() function.
// It then computes an acceleration value and prints the current position when changed.
// There is detailed output given to the Serial.
// You may play around with the constants to fit to your needs.

// Hardware setup:
// Attach a rotary encoder with output pins to
// * 2 and 3 on Arduino UNO.
// * A2 and A3 can be used when directly using the ISR interrupts, see comments below.
// * D5 and D6 on ESP8266 board (e.g. NodeMCU).
// Swap the pins when direction is detected wrong.
// The common contact should be attached to ground.
#include <Arduino.h>
#include <RotaryEncoder.h>



#define PIN_IN1 21 //pin n5
#define PIN_IN2 25 //pin m5


#include <SPI.h>
#include <Ethernet2.h>

#define SCK 22
#define MISO 23
#define MOSI 33
#define CS 19



byte mac[] = {  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
//IPAddress ip(10, 0, 0, 33);
EthernetClient client;

#include <OSCMessage.h>
EthernetUDP Udp;
//IPAddress outIp(239, 200, 200, 200);
IPAddress outIp(10, 0, 0, 255);
const unsigned int outPort = 10000;

#include "M5Atom.h"
uint8_t DisBuff[2 + 5 * 5 * 3]; //couleur du bouton?
void setBuff(uint8_t Rdata, uint8_t Gdata, uint8_t Bdata)
{
  DisBuff[0] = 0x05;
  DisBuff[1] = 0x05;
  for (int i = 0; i < 25; i++)
  {
    DisBuff[2 + i * 3 + 0] = Rdata;
    DisBuff[2 + i * 3 + 1] = Gdata;
    DisBuff[2 + i * 3 + 2] = Bdata;
  }
}


// Setup a RotaryEncoder with 4 steps per latch for the 2 signal input pins:
// RotaryEncoder encoder(PIN_IN1, PIN_IN2, RotaryEncoder::LatchMode::FOUR3);

// Setup a RotaryEncoder with 2 steps per latch for the 2 signal input pins:
RotaryEncoder encoder(PIN_IN1, PIN_IN2, RotaryEncoder::LatchMode::TWO03);

// Define some constants.

// the maximum acceleration is 10 times.
constexpr float m = 10;

// at 200ms or slower, there should be no acceleration. (factor 1)
constexpr float longCutoff = 500;

// at 5 ms, we want to have maximum acceleration (factor m)
constexpr float shortCutoff = 5;

// To derive the calc. constants, compute as follows:
// On an x(ms) - y(factor) plane resolve a linear formular factor(ms) = a * ms + b;
// where  f(4)=10 and f(200)=1

constexpr float a = (m - 1) / (shortCutoff - longCutoff);
constexpr float b = 1 - longCutoff * a;

// a global variables to hold the last position
static int lastPos = 0;
float ticksActual_float;
long deltaTicks;

void setup()
{
  SPI.begin(SCK, MISO, MOSI, -1);
  Ethernet.init(CS);
  M5.begin(true, false, true);
  delay(10);
  setBuff(0x99, 0x99, 0x99);
  M5.dis.displaybuff(DisBuff);
  Serial.println("Serial Connection initialized");
  setBuff(0x00, 0x99, 0x99);
  M5.dis.displaybuff(DisBuff);
  //Ethernet.begin(mac, ip);
  Ethernet.begin(mac);
  //Udp.begin(outPort);
  Udp.beginMulticast(outIp, outPort);
  setBuff(0x00, 0x00, 0x99);
  M5.dis.displaybuff(DisBuff);
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());

} // setup()


// Read the current position of the encoder and print out when changed.
void loop()
{
  encoder.tick();
  //int newPos = encoder.getPosition();
  encoder_new_pos(encoder.getPosition());
  M5.update();
  if (M5.Btn.wasPressed())
  {
    Serial.println("click");
    OSCMessage msg("/button/");
    msg.add(1);
    Udp.beginPacket(outIp, outPort);
    msg.send(Udp); //
    Udp.endPacket();
    msg.empty(); // 

  }
  if (M5.Btn.wasReleased())
  {
    Serial.println("unclick");
    OSCMessage msg("/button/");
    msg.add(0);
    Udp.beginPacket(outIp, outPort);
    msg.send(Udp); //
    Udp.endPacket();
    msg.empty(); // 
  }

} // loop ()


void encoder_new_pos(int newPos)
{
  if (lastPos != newPos)
  { // accelerate when there was a previous rotation in the same direction.
    unsigned long ms = encoder.getMillisBetweenRotations();

    ticksActual_float = a * ms + b;
    //      Serial.print("  f= ");
    //      Serial.println(ticksActual_float);

    deltaTicks = (long)ticksActual_float * (newPos - lastPos);
    //      Serial.print("  d= ");
    //      Serial.println(deltaTicks);

    newPos = newPos + deltaTicks;
    encoder.setPosition(newPos);

    Serial.print(newPos);
    Serial.print("  ms ");
    Serial.print(ms);
    Serial.print("  f ");
    Serial.print(ticksActual_float);
    Serial.print("  d ");
    Serial.println(deltaTicks);
 
    
    OSCMessage msg("/encoder/");
    msg.add((int32_t)newPos-lastPos);
    Udp.beginPacket(outIp, outPort);
    msg.send(Udp); //
    Udp.endPacket();
    msg.empty(); // 
    lastPos = newPos;

  }
}
// The End

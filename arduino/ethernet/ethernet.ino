/*
    Description: 
    Use ATOM PoE to connect to the LAN and start the webserver, 
    users can connect to the IP address, and control the LEDs through the web page.
    Please install library before compiling:  
    FastLED: https://github.com/FastLED/FastLED
    Ethernet2: https://github.com/adafruit/Ethernet2
*/

#include <SPI.h>
#include <Ethernet2.h>



#define SCK 22
#define MISO 23
#define MOSI 33
#define CS 19



// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(10, 0, 0, 33);

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
//EthernetServer server(80);

void setup() 
{
  SPI.begin(SCK, MISO, MOSI, -1);
  Ethernet.init(CS);

  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Ethernet WebServer Example");

  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  // start the server
  //server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
}


void handler() 
{

}

void loop() 
{

}

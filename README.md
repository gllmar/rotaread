# Rotaread

rotation encoder reader

[[_TOC_]]

## Fonction

Read and communicate relative rotation of rotary encoder


## Components

![test_gitlab.drawio](composantes.drawio.png)


### MCU (microcontroller)

* m5atom POE 

#### [M5StackAtom POE](https://docs.m5stack.com/en/core/atom_lite)

* ![m5Stack atom schématique](https://static-cdn.m5stack.com/resource/docs/static/assets/img/product_pics/core/minicore/atom/ATOM_LITE_SIMPLE_CIRCUT_20200514.webp)

##### Groove connector
|pin| color|
|--|----|
|32|Blanc|
|26|Jaune|
|5V|Rouge|
|GND|Noir|


### Rotary encoder

#### Incremental Rotary Encoder

![](docs/rotary_encoder.drawio.png)


#### Color code

|pin| color|
|--|--|
|green | A phase|
|white | B phase|
|red | Vcc power + |
| black | V0 |


### MCU
```mermaid
graph LR
subgraph MCU
MCU_5v[5V]
MCU_GND[GND]
MCU_2[2]
MCU_3[3]
end
subgraph Rotary Encoder
rot_5V[5v]
rot_GND[GND]
rot_PHASE_A[A]
rot_PHASE_B[B]
end
MCU_5v-->rot_5V
MCU_GND-->rot_GND
MCU_2-->rot_PHASE_A
MCU_3-->rot_PHASE_B
```



### knob

#### uxcell a15020500ux0472

![uxcell a15020500ux0472](./docs/uxcell_a15020500ux0472.drawio.png)

https://www.amazon.ca/gp/product/B0147XGEFA/

* Potentiometer Base: 20mm
* Shaft Max.Length: 15mm
* Shaft Diameter: 6mm
* 38 x 22mm (D*H)


#### uxcell a15020500ux0475

![uxcell a15020500ux0475](docs/uxcell_a15020500ux0475.drawio.png)

https://www.amazon.ca/gp/product/B00XXHJPLC/

* Shaft ø = 6mm
* Knob ø  = 35mm 
* Knob height = 16mm
